MODULES      = Finder    Finder::Group   Finder::Hasher   Finder::Looper
MODULE_FILES = Finder.pm Finder/Group.pm Finder/Hasher.pm Finder/Looper.pm
VERSION      = 1.3
DISTFILE     = dupseek-$(VERSION).tgz
COPYRIGHT    = copyright
DISTDIR      = dupseek-$(VERSION)

all: dupseek

dupseek: dupseek_modular.pl $(COPYRIGHT) $(MODULE_FILES)
	./make_self_contained.pl $< $(COPYRIGHT) $(MODULES) > $@
	chmod a+x $@

dist: $(DISTFILE)

$(DISTFILE): dupseek $(COPYRIGHT) changelog.txt  credits.txt  doc.txt Makefile make_self_contained.pl dupseek_modular.pl maketests.pl $(MODULE_FILES)
	mkdir $(DISTDIR)
	for a in $^; do cp --parents $$a $(DISTDIR); done
	replace 1.3 $(VERSION) -- `find $(DISTDIR) -type f`
	tar czvf $@ $(DISTDIR) && rm -rf $(DISTDIR)

doc: ~/wml/beautylabs/static/software/dupseek.html
	html2text -nobs $^ | grep -v "MANIFEST type html" > doc.txt
