#!/usr/bin/perl -w

#copyright

#modules

package main;

use strict;
use Getopt::Std;
use Finder;

my %opt;
my %fmt;

# Whether symbolic links can be created on the system
my $hasSymlinks = eval 'symlink("",""); use File::Spec; 1';

#----------------------------------------
# readOption()
# read a command and return chomped string
#----------------------------------------
sub readOption {
    chomp ( $_ = <STDIN> );
    $_;
}

sub terminal {
    my $self = shift;
    for ( @_ ) {
	if ($_ -> files > 1) {
	    processGroup( $_ );
	}
    }
}

#----------------------------------------
# processGroup
# interactively process a group
# return false if the group needs further processing
#----------------------------------------
sub processGroup {
    my $group = shift;
    my $size = $group -> size;

    # Print header if interactive or batch and indicated in format
    if ($fmt{h} || ! $opt{b}) {
	print $group -> progress, "\n" unless $opt{b};
	print ( $group -> isTerminal ? "Duplicate " : "Possibly duplicate " );
	print "files of size $size bytes:\n";
    }

    while (1) {
	my @file = $group -> files;
	
	$opt{b} = 'delete' if $opt{b} eq 'kill';

	if ( $opt{b} eq 'report' ) {
	    for ( 0 .. $#file ) {
		next if ( $fmt{d} && ! $_ );
		my $file = $file[$_];
		$file = quotemeta($file) if $fmt{e};
		print "$file\n";
	    }
	    print "\n" if $fmt{n};
	    return 1;
	}

	my $input;

	# Interact with user unless in batch mode
	unless ( $opt{b} ) {

	    for (0..$#file) {
		printf "[ %2d ] %s\n", $_, $file[$_];
	    }

	    print "\n",
	    "[return] continue [Q] quit the program\n",
	    "[k0...k$#file] keep one file and remove the rest\n";

	    # Read acceptable input
	    my %option = ( ( map { ( "k$_" => 1 ) } (0..$#file) ),
			   ( map { ( $_    => 1 ) } ('', 'Q') ) );

	    if ( $hasSymlinks ) {
		print "[l0...l$#file] keep one file and substitute the rest with symbolic links\n";
		$option{"l$_"} = 1 for (0..$#file);
	    }
	    
	    until ( $option{ $input = readOption } ) {
		print "Wrong option $input\n";
	    }

	    # Dispatch action according to command
	    if ($input eq '') {
		return $group -> isTerminal;
	    }
	}

	if ($opt{b} eq 'delete' || $opt{b} eq 'link' || $input =~ m/(k|l)([0-9]+)/) {
	    my $ok = 1;

	    my ( $cmd, $index ) = ( $1, $2 );
	    $index = 0 if $opt{b};
	    $cmd = 'l' if $opt{b} eq 'link';
	    $cmd = 'k' if $opt{b} eq 'delete';

	    for (0..$#file) {
		my $delendum = $file [$_];
		if ($_ == $index) {
		    print STDERR "Keeping $delendum\n";
		    next;
		}
		print STDERR "Unlinking $delendum: ";
		if ( unlink $delendum ) {
		    print STDERR "done\n";
		} else {
		    $ok = 0;
		    print STDERR "ERROR: $!\nskipping file $delendum in elaboration\n";
		    $group -> remove( $delendum );
		}
		if ($cmd eq 'l') {
		    print STDERR "Making symbolic link from $delendum to $file[$index]: ";
		    if ( symlink File::Spec -> rel2abs( $file[$index] ), $delendum ) {
			print STDERR "done\n";
		    } else {
			$ok = 0;
			print STDERR "ERROR: $!\nskipping file $delendum in elaboration\n";
			$group -> remove( $delendum );
		    }
		}
	    }
	    return 1 if $ok || $opt{b};
	    next;

	} elsif ($input eq 'Q') {

	    exit 0;

	}
    }
    return 1;
}    





getopts('hb:f:m:M:',\%opt);


$opt{b} ||= '';
if ( $opt{b} eq 'report' ) {
    my $format = $opt{f};
    $format = 'full' unless defined $format;
    $format = { full   => 'hn',
		simple => 'n',
		xargs  => 'dne' } -> { $format } || $format;
    %fmt = ( map ( ( $_ => 1 ), split ( //, $format ) ) );
    $fmt{s} = 1 unless $fmt{e};
} else {
    %fmt = ();
}


if ($opt{h} || !@ARGV) {
  print <<END;
Dupseek ver. 1.3
Recursively scan one or more directories for duplicate files.

Usage $0 [-h] [ -m <min_size> ] [ -M <max_size> ] [-b <batch_mode>] [-f <format>] <directory> ...
  -h Print this help message
  -m <min_size> Ignore files smaller than min_size
  -M <max_size> Ignore files larger than max_size
     min_size and max_size accept k, m and g modifiers
     for kilo, mega, and gigabytes. Default is bytes

  -b <batch_mode> Automatically perform actions, without
     prompting the user interactively.
     batch_mode can be one of
     report: Print a report on the duplicates found.
             The format defaults to 'full' but can be
             specified by -f format
     delete: Keep the first file of every group of
             duplicates and delete the rest
     kill:   Equivalent to 'delete'
     link:   Like delete, but substitute duplicates
             with symbolic links to the first file
  -f <format> Output format for report batch mode.
     format can be expressed in mnemonic form or by
     a sequence of flag characters.

     Flags are
     d (duplicates): Skip the first file in any group
     h (header):     Print some information on each group
     n (newline):    Separate groups with a newline
     s (simple):     Print filenames
     e (escape):     Escape special characters in file names

     A mnemonic form can be one of
     full   = hn   For humans to read
     simple = n    For automatic parsing
     xargs  = dne  The output can be piped through xargs for
                   advanced removal or other processing
     
SAMPLE USAGE:
- Interactive search:
  $0 dir1 dir2
- Same as above for files of size at least 1 kilobyte:
  $0 -m 1k dir1 dir2
- Same as above for files of size at most half a gigabyte:
  $0 -M .5g dir1 dir2
- Keep only one copy of duplicate files:
  $0 -b kill dir1 dir2
- Substitute duplicates with symbolic links:
  $0 -b link dir1 dir2
- Use backtick expansion to move duplicates to /trash:
  mv `$0 -b report -f de dir1 dir2` /trash
- As above, but with xargs:
  $0 -b report -f xargs dir1 dir2 | xargs -i mv {} /trash

Pressing Ctrl-C during interactive processing, you will be presented
with partial results.

END
  exit(0);
}

$opt{b} = 1 if $opt{o};

# Check and adjust min and max size
for my $o ( 'm', 'M' ) {
  if ( $opt{$o} ) {
    if ( $opt{$o} =~ m/^([0-9.]+)([kmg])?$/i ) {
      $opt{$o} = 0 + $1;
      if ( $2 ) {
         $opt{$o} *= { k => 1<<10, m => 1<<20, g => 1<<30 } -> { lc $2 };
      }
    } else {
      die "Invlid size $opt{$o}\n";
    }
  }
}

my $finder = Finder -> new( @ARGV );
$finder -> setTerminalAction( \&terminal );
$finder -> setMinSize ( $opt{m} ) if ( $opt{m} );
$finder -> setMaxSize ( $opt{M} ) if ( $opt{M} );
$finder -> readDirs();


my $nextTerminal = 0;
my $step = 0;

my $interrupted = 0;

$SIG{INT} = sub { $interrupted++ } unless $opt{b};

my @w;
while ( @w = $finder -> waiting ) {
    if ($interrupted) {
        $finder -> processWaiting( \&processGroup );
    }
    $interrupted = 0;
    $finder -> iterate();
}
