#!/usr/bin/perl -w

use strict;


my ( $script, $copyright, @module ) = @ARGV;

sub readFile {
    my $result;
    open FILE, shift;
    read FILE, $result, 1e6;
    close FILE;
    $result;
}

$script = readFile( $script );
$copyright = readFile( $copyright );

$script =~ s/\#copyright/$copyright/g;

my $modules = '';
for ( @module ) {
    my $file = $_;
    $file =~ s|::|/|g;
    $file .= '.pm';
    $modules .= "\n#" . '-' x 60 . "\n# package $_\n#" . '-' x 60 . "\n"
	. readFile( $file );
}

$script =~ s/\#modules/$modules/;

for ( @module ) {
    $script =~ s/(use\s*$_;)/\# $1/g;
}

print $script;
