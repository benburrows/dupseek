#!/usr/bin/perl -w

use strict;

use Getopt::Long;

GetOptions(
	   'max=i' => \( my $max = 1E6 ),
    'num=i' => \( my $num = 10 ), 
    'block=i' => \( my $block = 10 ),
    'cheap!' => \( my $cheap = 1 )
    );

for my $outer (1..$num) {
    my $size = int exp rand log $max;
    my $n = 1 + rand $block;
    my @pos = map { int rand $size } (1..$n);
    for my $inner (1..$n) {
	my $pos = $pos[ rand( @pos ) ];
	open FILE, "> testfile_${size}_${pos}_${inner}";
	if ($cheap) {
	    seek FILE, $pos, 0;
	    print FILE '1';
	    seek FILE, $size-1, 0;
	    print FILE '0';
	} else {
	    print FILE 'a' x $pos, 'X', 'b' x ( $size - $pos - 1 );
	}
    }
}
