package Finder::Group;

#------------------------------------------------------------
# Finder::Group
#------------------------------------------------------------
# Group of files which might be the same
#------------------------------------------------------------

use strict;

use Finder::Hasher;

#----------------------------------------
# new( [ group [ , newkey => val ] ] )
#----------------------------------------
sub new {
    my $class = shift;
    my ($parent, %key) = @_;
    my $self = bless { files => [] }, $class;
    if ($parent) {
	$self -> {keys} = { %{ $parent -> {keys} }, %key };
	$self -> {hasher} = $parent -> hasher -> next( $self );
    } else {
	$self -> {keys} = {};
	$self -> {hasher} = Finder::Hasher -> new();
    };
    return $self;
}

#----------------------------------------
# hasher()
# return hasher
#----------------------------------------
sub hasher { shift -> {hasher} }

#----------------------------------------
# files()
# return file list
#----------------------------------------
sub files {
    my $self = shift;
    return @{ $self -> {files} };
}

#----------------------------------------
# remove (filename)
# remove filename from group
#----------------------------------------
sub remove {
    my $self = shift;
    my ($file) = @_;
    $self -> {files} = grep { $_ ne $file } @{ $self -> {files} };
}
    
#----------------------------------------
# progress()
# return hashing progress string
#----------------------------------------
sub progress {
    my $self = shift;
    my $hasher = $self -> hasher or return "";
    return $hasher -> progress;
}

#----------------------------------------
# isTerminal()
# return 1 if the group is terminal
# undef otherwise
#----------------------------------------
sub isTerminal {
    my $self = shift;
    return $self -> files() <= 1 || ! defined( $self -> hasher );
}

#----------------------------------------
# <keyName>()
# autoload tries to return the corresponding key value
# ex. $group->size() returns $group->{keys}{size}
#----------------------------------------
sub AUTOLOAD {
    my $self = shift;
    my $par = $Finder::Group::AUTOLOAD;
    my @par = split '::', $par;
    return $self -> {keys} {pop @par};
}

#----------------------------------------
# add( file, ... )
#----------------------------------------
sub add {
    my $self = shift;
    push @{ $self -> {files} }, @_;
}

#----------------------------------------
# process( file )
# return hash applied to file
#----------------------------------------
sub process {
    my $self = shift;
    my ($fileName) = @_;
    return $self -> hasher -> process( $fileName );
}

#----------------------------------------
# partition()
# execute a discrimination step
# return list of groups
#----------------------------------------
sub partition {
    my $self = shift;

    my %bucket = ();

    for ($self -> files) {
	my $hash = $self -> process( $_ );
	push @{ $bucket {$hash} ||= [] }, $_;
    }

    my @result = ();

    for (keys %bucket) {
	my %key;
	my $name = $self -> hasher -> name;
	$key{ $name } = $_ if $name;
	my $newGroup = ref($self) -> new( $self, %key );
	$newGroup -> add( @{ $bucket{$_} } );
	push @result, $newGroup;
    }

    return @result;
}


1;
