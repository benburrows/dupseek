package Finder::Looper;

#------------------------------------------------------------
# Finder::Looper
#------------------------------------------------------------
# Iterator providing starting points and lengths
# for interlaced reads
#------------------------------------------------------------

use strict;
use constant STEP => 2; # Increase factor


#----------------------------------------
# new( size [, minsize [, maxsize ]] )
#----------------------------------------
sub new {
    my $class = shift;
    my ( $size, $minsize, $maxsize ) = @_;
    $minsize ||= 1;
    $maxsize ||= 2**16;
    bless {
	size     => $size,
	minsize  => $minsize,
	maxsize  => $maxsize,
	readsize => $minsize,
	oldsize  => 0,
	i        => 0,
	gap      => 1 << nextLog2( $size ),
	progress => 0
    }, $class;
}

#----------------------------------------
# clone()
# return a clone of the looper
#----------------------------------------
sub clone {
    my $self = shift;
    bless { %$self }, ref $self;
}

#----------------------------------------
# next()
# return ( start, length )
# return () if the iteration is over
#----------------------------------------
sub next {
    my $self = shift;

    if ( $self -> {i} * $self -> {gap} >= $self -> {size} ) {

	if ( $self -> {readsize} >= $self -> {gap} ) {
	    return ();
	}
	
	$self -> {i} = 0;
	$self -> {oldsize} = $self -> {readsize};
	$self -> {gap} >>= 1;
	$self -> {readsize} *= STEP;
	$self -> {readsize} = $self -> {gap}
	if ($self -> {readsize} > $self -> {gap});
	$self -> {readsize} = $self -> {maxsize}
	if ($self -> {readsize} > $self -> {maxsize});
    }

    my $offset = ( $self -> {i} % 2 ) ? 0 : $self -> {oldsize};
    
    my $start  = $self -> {i} * $self -> {gap} + $offset;
    my $length = $self -> {readsize} - $offset;
    $length    = $self -> {size} - $start
	if $start + $length > $self -> {size};

    $self -> {i} ++;

    if ( $length <= 0 ) {
	return $self -> next();
    } else {
	$self -> {progress} += $length;
	return ( $start, $length );
    } 
}

#----------------------------------------
# function
#----------------------------------------
# nextLog2( positive integer )
# return exponent of nearest power of 2
# not less than integer
# Warning: returns at most the biggest power of
# two expressed by an integer
#----------------------------------------
sub nextLog2 {
    my $i = shift;
    my $pow = 1;
    my $exp = 0;
    while ( $pow < $i && $pow > 0 ) {
	$pow <<= 1;
	$exp++;
    }
    return $exp;
}



1;
