package Finder::Hasher;


#------------------------------------------------------------
# Finder::Hasher
# objects computing hash values for files
#------------------------------------------------------------

use strict;


#----------------------------------------
# new()
#----------------------------------------
sub new {
    Finder::Hasher::Size -> new();
}





#------------------------------------------------------------
# Finder::Hasher::Size
# compute file size
#------------------------------------------------------------

package Finder::Hasher::Size;

#----------------------------------------
# new()
#----------------------------------------
sub new {
    bless {}, __PACKAGE__;
}

#----------------------------------------
# process( filename )
#----------------------------------------
sub process {
    my $self = shift;
    size( @_ );
    
}

#----------------------------------------
# name()
# Return the name (the hash is meaningful)
#----------------------------------------
sub name { 'size' }

#----------------------------------------
# next( [ group ] )
# return next hasher
# return undef if hasher was terminal
#----------------------------------------
sub next {
    my $self = shift;
    my ($group) = @_;

    my $size = $group -> size;
    return undef if ($size==0);
    
    Finder::Hasher::Sample -> new( $size );
}

#----------------------------------------
# clone()
# return an identical twin
#----------------------------------------
sub clone {
    my $self = shift;
    return bless %$self, ref $self;
}

#----------------------------------------
# progress()
# return a string indicating progress
#----------------------------------------
sub progress {
    return "Size computed";
}

#----------------------------------------
# size ( filename )
# Find file size
#----------------------------------------
sub size {
    my $fname = shift;
    return (stat ($_))[7];
}









#------------------------------------------------------------
# Finder::Hasher::Sample
# Read samples from the file
#------------------------------------------------------------


package Finder::Hasher::Sample;

use IO::File;
use Finder::Looper;
use Finder::Group;

use constant MINREADSIZE  => 1024;
use constant MAXREADSIZE  => 1024 * 1024;
use constant BLOCK        => 4096;
use constant MAXOPENFILES => 64;

our %handle = ();
our $handles = 0;

#----------------------------------------
# new( size )
#----------------------------------------
sub new {
    my $class = shift;
    my ($size) = @_;

    my $iterator = Finder::Looper -> new( $size, MINREADSIZE, MAXREADSIZE );
    
    my ($start, $length) = $iterator -> next()
	or return undef;

    bless { iterator => $iterator,
	    start => $start,
	    length => $length
	    }, $class;
}

#----------------------------------------
# next( [ group ] )
# return next hasher
# return undef if hasher was terminal
#----------------------------------------
sub next {
    my $self = shift;

    my $iterator = $self -> {iterator} -> clone();

    my ($start, $length) = $iterator -> next()
	or return undef;

    bless { iterator => $iterator,
	    start => $start,
	    length => $length
	    }, ref $self;
}

#----------------------------------------
# name()
# Return undef (the hash is not meaningful)
#----------------------------------------
sub name { undef }

#----------------------------------------
# process( filename )
#----------------------------------------
sub process {
    my $self = shift;
    my ($file) = @_;
    sample( $file, $self -> {start}, $self -> {length} );
}

#----------------------------------------
# clone()
# return an identical twin
#----------------------------------------
sub clone {
    my $self = shift;
    my $clone = bless %$self, ref $self;
    $clone -> {iterator} = $clone -> {iterator} -> clone();
    return $clone;
}

#----------------------------------------
# progress()
# return a string indicating progress
#----------------------------------------
sub progress {
    my $self = shift;
    my $iterator = $self -> {iterator};
    my $prog = $iterator -> {progress} / $iterator -> {size};
    return (100 * $prog) . "% read";
}


#----------------------------------------
# function
#----------------------------------------
# fileHandle( filename )
# return fileHandle or undef
#----------------------------------------
sub fileHandle {
    my ($fileName) = @_;
    my $handle = $handle{$fileName};
    return $handle if $handle;

    if ($handles >= MAXOPENFILES ) {
	closeHandle( (keys %handle)[ rand $handles ] );
    }

    $handle = IO::File -> new();
    $handle -> open("<$fileName") || return undef;
    $handle{$fileName} = $handle;
    $handles++;

    return $handle;
}

#----------------------------------------
# function
#----------------------------------------
# closeHandle( filename )
# close handle
#----------------------------------------
sub closeHandle {
    for (@_) {
	delete $handle{$_};
	$handles--;
    }
}


{
    # $error is a counter to provide ever-changing names
    # as hash when incurring in hashing errors
    my $error = 0;

#----------------------------------------
# sample ( filename, [ start [, length ] ] )
#----------------------------------------
sub sample {
    my ($fname, $start, $length) = @_;
    $start ||= 0;

    my $res;

    # Return a consecutive error code if unable to open file
    my $handle = fileHandle( $fname ) || return "Error " . $error++;
    $handle -> seek( $start, 0 );

    if ($length) {
	$handle -> read( $res, $length );
    } else {
	$res = '';
	my $buffer;
	while ( $handle -> read( $buffer, BLOCK ) ) {
	    $res .= $buffer;
	}
    }

    return $res;
}
}


1;
