package Finder;

#------------------------------------------------------------
# dupfinder: find duplicate files
#------------------------------------------------------------

use strict;
use File::Find;
use Finder::Group;

#----------------------------------------
# new ( dir, ... )
# Create new finder
#----------------------------------------
sub new {
    my $class = shift;
    my $self = {
	dirs     => [ @_ ],
	waiting   => [],
	terminal => [],
	minSize => undef,
	maxSize => undef
    };
    return bless $self, $class;
}


#----------------------------------------
# setMinSize ( size )
#----------------------------------------
sub setMinSize {
    my $self = shift;
    my ( $size ) = @_;
    $self -> { minSize } = $size;
}

#----------------------------------------
# setMaxSize ( size )
#----------------------------------------
sub setMaxSize {
    my $self = shift;
    my ( $size ) = @_;
    $self -> { maxSize } = $size;
}

#----------------------------------------
# terminal()
# return terminal groups
#----------------------------------------
sub terminal {
    my $self = shift;
    return @{ $self -> { terminal } };
}

#----------------------------------------
# waiting()
# return waiting groups
#----------------------------------------
sub waiting {
    my $self = shift;
    return @{ $self -> { waiting } };
}

#----------------------------------------
# readDirs ()
# Find all files and setup finder
#----------------------------------------
sub readDirs {
    my $self = shift;
    my $group = Finder::Group -> new();
    my %seen_dirs = ();
    my $process = sub {
	my $mins = $self -> { minSize };
	my $maxs = $self -> { maxSize };
	my ( $dev, $node ) = lstat;
	if ( -d _ ) {
	    my $sd_key = "$dev:$node";
	    if ( exists $seen_dirs { $sd_key } ) {
		die "$seen_dirs{$sd_key} and $File::Find::name are the same directory. Refusing to proceed\n";
	    } else {
		$seen_dirs { $sd_key } = $File::Find::name;
		return;
	    }
	}
	if ( -f _ && ! -l _ ) {
	    return if defined $mins and -s _ < $mins;
	    return if defined $maxs and -s _ > $maxs;
	    $group -> add( $File::Find::name );
	    return;
	}
    };
    find( $process,
	  @{$self->{dirs}} );
    $self -> {waiting} = [ $group ];
}


#----------------------------------------
# iterate( [ n ] )
# Perform one or n iterations
# and add groups to terminal
#----------------------------------------
sub iterate {
    my $self = shift;
    my $n = shift || 1;
    
    my @group = @{ $self -> {waiting} };
    my @newGroup;

    for (1..$n) {
	@newGroup = ();
	for ( @group ) {
	    # Partition the group and position properly
	    # the resulting subgroups
	    my @split = $_ -> partition();
	    for (@split) {
		if ( $_ -> isTerminal ) {
		    $self -> addTerminal( $_ );
		} else {
		    push @newGroup, $_;
		}
	    }
	}
	@group = @newGroup;
    }

    $self -> {waiting} = \@newGroup;
    return @{ $self -> {terminal} };
}

#----------------------------------------
# setTerminalAction( sub { code } )
# Set action to perform on terminal groups
#----------------------------------------
sub setTerminalAction {
    my $self = shift;
    my ($code) = @_;
    $self -> {terminalAction} = $code;
}

#----------------------------------------
# addTerminal( group, ... )
# the group has been found to be terminal
# process it
#----------------------------------------
sub addTerminal {
    my $self = shift;
    my $sub = $self -> {terminalAction};
    if ($sub) {
	$sub -> ( $self, @_ );
    } else {
	push @{ $self -> {terminal} }, @_;
    }
}

#----------------------------------------
# processWaiting( code )
# execute code -> ( $group )
# for all waiting groups
# if the outcome is false, put back into the waiting list
#----------------------------------------
sub processWaiting {
    my $self = shift;
    my ($sub) = @_;
    my @newWaiting = ();
    for my $group ($self -> waiting) {
	unless ( $sub -> ( $group ) ) {
	    push @newWaiting, $group;
	}
    }
    $self -> {waiting} = \@newWaiting;
}

1;
